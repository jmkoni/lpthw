from sys import argv

# input is the script and then the new filename
script, filename = argv

# Gives the user instructions
print "We're going to erase %r." % filename
print "If You don't want that, hit CTRL-C (^C)."
print "If you do want that, hit RETURN."

# Requests user input
raw_input("?")

# opens file user specified
print "Opening the file..."
target = open(filename, 'w')

# deletes file
print "Truncating the file. Goodbye!"
target.truncate()

print "Now I'm going to ask you for three lines."

# gets user input as to what to put in the file
line1 = raw_input("line 1: ")
line2 = raw_input("line 2: ")
line3 = raw_input("line 3: ")

print "I'm going to write these to the file."

# Writes lines and spaces
target.write(line1 + "\n" + line2 + "\n" + line3 + "\n")
#target.write("\n")
#target.write(line2)
#target.write("\n")
#target.write(line3)
#target.write("\n")

# closes file
print "And finally, we close it."
target.close()
